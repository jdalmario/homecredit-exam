package com.jdalmario.homecredit.domain;

import com.google.gson.annotations.SerializedName;

public class Temperature {

    private double temp;
    private double pressure;
    private double humidity;
    @SerializedName("temp_min")
    private double tempMin;
    @SerializedName("temp_max")
    private double tempMax;

    public double getTemp() {
        return temp;
    }
}
