package com.jdalmario.homecredit.domain;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Weather {

    private long id;

    @SerializedName("coord")
    private Coordinate location;
    @SerializedName("weather")
    private List<ActualWeather> actualWeather;
    @SerializedName("main")
    private Temperature temperature;

    @SerializedName("name")
    private String name;

    @SerializedName("dt")
    private long dateTime;


    public List<ActualWeather> getActualWeather() {
        return actualWeather;
    }

    public void setActualWeather(List<ActualWeather> actualWeather) {
        this.actualWeather = actualWeather;
    }

    public String getName() {
        return name;

    }

    public void setName(String name) {
        this.name = name;
    }

    public Coordinate getLocation() {
        return location;
    }

    public void setLocation(Coordinate location) {
        this.location = location;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "id=" + id +
                ", location=" + location +
                ", actualWeather=" + actualWeather +
                ", temperature=" + temperature +
                ", name='" + name + '\'' +
                ", dateTime=" + dateTime +
                '}';
    }
}
