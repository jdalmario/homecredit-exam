package com.jdalmario.homecredit.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "WeatherLog")
public class WeatherLog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private long id;
    @JsonIgnore
    @Column(unique = true)
    private String responseId;
    private String location;
    private String actualWeather;
    private String temperature;
    private Timestamp dtimeInserted;

    public WeatherLog(String responseId, String location, String actualWeather, String temperature) {
        this.responseId = responseId;
        this.location = location;
        this.actualWeather = actualWeather;
        this.temperature = temperature;
    }

    protected WeatherLog() {

    }

    /**
     * Adapter pattern
     *
     * @param weather
     */
    public WeatherLog(Weather weather) {
        this.responseId = weather.getId() + "";
        this.location = weather.getName();
//            We assume the first
        this.actualWeather = weather.getActualWeather().get(0).getMain();
        this.temperature = weather.getTemperature().getTemp() + "";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getActualWeather() {
        return actualWeather;
    }

    public void setActualWeather(String actualWeather) {
        this.actualWeather = actualWeather;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    @PrePersist
    public void onPrePersist() {
        this.dtimeInserted = new Timestamp(System.currentTimeMillis());
    }
}
