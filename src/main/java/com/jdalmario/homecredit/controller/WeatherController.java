package com.jdalmario.homecredit.controller;

import com.jdalmario.homecredit.domain.Weather;
import com.jdalmario.homecredit.domain.WeatherLog;
import com.jdalmario.homecredit.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    @RequestMapping(value = "/weathers", method = RequestMethod.GET)
    public List<WeatherLog> getWeathers() {
        return weatherService.getWeatherDatas();
    }

    @RequestMapping(value = "/weather/{weatherId}", method = RequestMethod.GET)
    public WeatherLog getWeather(@PathVariable(value = "weatherId") int weatherId) {
        return weatherService.getWeatherLog(weatherId);

    }
}
