package com.jdalmario.homecredit.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.jdalmario.homecredit.domain.Weather;
import com.jdalmario.homecredit.domain.WeatherLog;
import com.jdalmario.homecredit.repository.WeatherLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeatherService {

    @Value("${app.id}")
    private String apiId;

    @Value("${api.url}")
    private String apiURL;

    @Autowired
    private Gson gson;

    private WeatherLogRepository weatherLogRepository;

    @Autowired
    public WeatherService(WeatherLogRepository weatherLogRepository) {
        this.weatherLogRepository = weatherLogRepository;
    }

    public List<WeatherLog> getWeatherDatas() {
        RestTemplate restTemplate = new RestTemplate();
        String urlCall = apiURL + "group?id=2643743,3067696,5391959" + "&appid=" + apiId;
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(urlCall, String.class);
        List<WeatherLog> weatherLogs = new ArrayList<>();

        JsonObject jsonObject = gson.fromJson(responseEntity.getBody(), JsonObject.class);
        List<Weather> weathers = gson.fromJson(jsonObject.get("list"), new TypeToken<List<Weather>>() {
        }.getType());

        weathers.forEach(weather -> weatherLogs.add(saveWeatherLog(new WeatherLog(weather))));
        return weatherLogs;
    }


    public WeatherLog getWeatherLog(int id) {
        RestTemplate restTemplate = new RestTemplate();
        String urlCall = apiURL + "weather?id=" + id + "&appid=" + apiId;
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(urlCall, String.class);
        Weather weather = gson.fromJson(responseEntity.getBody(), Weather.class);
        return saveWeatherLog(new WeatherLog(weather));
    }

    private WeatherLog saveWeatherLog(WeatherLog weatherlog) {
        if (weatherLogRepository.findByResponseId(weatherlog.getResponseId()) == null) {
            weatherLogRepository.save(weatherlog);
        }
        return weatherlog;
    }
}
