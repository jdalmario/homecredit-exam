package com.jdalmario.homecredit.repository;

import com.jdalmario.homecredit.domain.WeatherLog;
import org.springframework.data.repository.CrudRepository;

public interface WeatherLogRepository extends CrudRepository<WeatherLog, Long> {
    WeatherLog findByResponseId(String responseId);
}
