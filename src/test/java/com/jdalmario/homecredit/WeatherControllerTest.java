package com.jdalmario.homecredit;

import com.jdalmario.homecredit.controller.WeatherController;
import com.jdalmario.homecredit.domain.WeatherLog;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherControllerTest {

    @Autowired
    WeatherController weatherController;

    @Test
    public void testGetWeathers() {
        List<WeatherLog> weatherLog = weatherController.getWeathers();
        assert weatherLog.size() == 3;
    }

    @Test
    public void testGetWeather() {
        WeatherLog weatherLog = weatherController.getWeather(5391959);
        assert weatherLog.getLocation().equals("San Francisco");
    }
}
